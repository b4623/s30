const express = require("express");

const mongoose = require("mongoose");

const app = express();

const port = 3001;

mongoose.connect('mongodb+srv://dbUser:dbUser@zuitt.ri5rh.mongodb.net/batch164_to-do?retryWrites=true&w=majority', 
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
)

let db = mongoose.connection;


db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database"))


app.use(express.json());

app.use(express.urlencoded({ extended:true }));


const userSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		
		default: "pending"
	}
})

app.post("/signup", (req, res) => {
	console.log(req.body);
	//validation
	//If contents of the req.body with the property username and password is not empty, then push the data to the users array. else, please input both username and password
	if(req.body.username !== '' && req.body.password !== '') {
		user.push(req.body);
		res.send(`User ${req.body.username} successfully registered`)
	} else{
		res.send("Please input BOTH username and password")
	}
})
let user = []

app.get("/users", (req, res) => {
	User.findOne({ name: req.body.name }, (err, result) => {



		// Check if there are duplicate tasks
		// "findOne" is a Mongoose method that acts similar to "find" of MongoDB
		// findOne() returns the first document that matches the search criteria
		// If there are no matches, the value of result is null
		// "err" is a shorthand naming convention for errors

		//If a document was found and the document's name matches the information sent via the client
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate User found");
		} else{
			//if no document was found
			//create a new task object and save it to the database
			let newUser = new User({
				name: req.body.name
			});

			// The "save" method will store the information to the database
			// Since the "newTask" was created/instantiated from the Mongoose Schema it will gain access to this method to save to the database
			// The "save" method will accept a callback function which stores any errors found in the first parameter
			// The second parameter of the callback function will store the newly saved document
			// Call back functions in mongoose methods are programmed this way to store any errors in the first parameter and the returned results in the second parameter
			newUser.save((saveErr, savedUser) => {
				//if there are errors in saving
				if(saveErr){

					// Will print any errors found in the console
					// saveErr is an error object that will contain details about the error
					// Errors normally come as an object data type
					return console.error(saveErr)
				} else {
					// Return a status code of 201 for created
					// Sends a message "New task created" on successful creation
					return res.status(201).send("New User created")
				}
				user.push(req.body)


			})


		}
	})
})



app.listen(port, ()=> console.log(`Server running at port ${port}`));
